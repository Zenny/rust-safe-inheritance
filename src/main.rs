enum Value<'a> {
    Gerp(&'a mut Gerp),
    Merp(&'a mut Merp)
}

struct Gerp {
    name: &'static str
}

struct Merp {
    name: &'static str,
    abc: &'static str
}

trait Herp {
    fn say(&self);
    fn type_of(&mut self) -> &'static str;
    fn get(&mut self) -> Value;
}

impl Herp for Gerp {
    fn say(&self) {
        println!("Do a thing Gerp! {}", self.name);
    }

    fn type_of(&mut self) -> &'static str {
        "Gerp"
    }

    fn get(&mut self) -> Value {
        Value::Gerp(self)
    }
}

impl Herp for Merp {
    fn say(&self) {
        println!("Merp is best! {} {}", self.name, self.abc);
    }

    fn type_of(&mut self) -> &'static str {
        "Merp"
    }

    fn get(&mut self) -> Value {
        Value::Merp(self)
    }
}

struct Derp {
    vec: Vec<Box<Herp>>
}

impl Derp {
    fn get_first(&mut self, _type: &str) -> Option<&mut Box<Herp>> {
        for t in &mut self.vec {
            if t.type_of() == _type {
                return Some(t);
            }
        }
        None
    }
}

fn main() {
    let mut a = Derp {
        vec: Vec::new()
    };

    a.vec.push(Box::new(Gerp { name: "Beep" }) );
    a.vec.push(Box::new(Merp { name: "Meep", abc: "Test" }) );

    for thing in &mut a.vec {
        thing.say();
        if let Value::Merp(m) = thing.get() {
            //let merp: &mut Merp = m.unwrap();
            m.abc = "Hello!";
            println!("{}", m.abc);
        }
    }
    {
        let f = a.get_first("Merp");
        if f.is_some() {
            if let Value::Merp(m) = f.unwrap().get() {
                m.say();
                m.abc = "WOW!";
            }
        } else {
            println!("None found");
        }
    }
    {
        let f = a.get_first("Merp");
        if f.is_some() {
            if let Value::Merp(m) = f.unwrap().get() {
                m.say();
            }
        } else {
            println!("None found");
        }
    }

}
