struct Gerp {
    name: &'static str
}

struct Merp {
    name: &'static str,
    abc: &'static str
}

trait Herp {
    fn say(&self);
    fn type_of(self) -> &'static str;
    fn to_gerp(&mut self) -> Option<&mut Gerp>;
    fn to_merp(&mut self) -> Option<&mut Merp>;
}

impl Herp for Gerp {
    fn say(&self) {
        println!("Do a thing Gerp! {}", self.name);
    }

    fn type_of(self) -> &'static str {
        "Gerp"
    }

    fn to_gerp(&mut self) -> Option<&mut Gerp> {
        Some(self)
    }

    fn to_merp(&mut self) -> Option<&mut Merp> {
        None
    }
}


impl Herp for Merp {
    fn say(&self) {
        println!("Merp is best! {} {}", self.name, self.abc);
    }

    fn type_of(self) -> &'static str {
        "Merp"
    }

    fn to_gerp(&mut self) -> Option<&mut Gerp> {
        None
    }

    fn to_merp(&mut self) -> Option<&mut Merp> {
        Some(self)
    }
}

struct Derp {
    vec: Vec<Box<Herp>>
}

fn main() {
    let mut a = Derp {
        vec: Vec::new()
    };

    a.vec.push(Box::new(Gerp{ name: "Beep" }));
    a.vec.push(Box::new(Merp{ name: "Meep", abc: "Test" }));

    for thing in &mut a.vec {
        thing.say();
        let m = thing.to_merp();
        if m.is_some() {
            let merp = m.unwrap();
            merp.abc = "Hello!";
            println!("{}", merp.abc);
        }
    }
    for thing in &mut a.vec {
        thing.say();
    }

}

